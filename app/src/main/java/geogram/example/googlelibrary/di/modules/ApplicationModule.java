package geogram.example.googlelibrary.di.modules;

import android.app.Application;
import android.content.Context;
import android.net.Uri;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.UrlConnectionDownloader;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geogr on 15.05.2018.
 */

@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application1) {
        this.application = application1;

    }

    @Singleton
    @Provides
    public Context provideContext() {
        return application;
    }

}
