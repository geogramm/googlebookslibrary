package geogram.example.googlelibrary.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import geogram.example.googlelibrary.rest.RestClient;
import geogram.example.googlelibrary.rest.api.BooksService;

/**
 * Created by geogr on 15.05.2018.
 */
@Module
public class RestModule {
    private RestClient restClient;

    public RestModule() {
        restClient = new RestClient();
    }

    @Singleton
    @Provides
    public RestClient providesRestClient() {
        return restClient;
    }

    @Singleton
    @Provides
    public BooksService getImages() {
        return restClient.createService(BooksService.class);
    }

}