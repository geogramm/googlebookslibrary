package geogram.example.googlelibrary.di.component;

import javax.inject.Singleton;
import javax.sql.DataSource;

import dagger.Component;
import geogram.example.googlelibrary.adapters.BooksListAdapter;
import geogram.example.googlelibrary.di.modules.ApplicationModule;
import geogram.example.googlelibrary.di.modules.RestModule;
import geogram.example.googlelibrary.ui.fragments.BooksListFragment;

/**
 * Created by geogr on 15.05.2018.
 */


@Singleton
@Component(modules = {ApplicationModule.class, RestModule.class })
public interface ApplicationComponent {
    void inject(BooksListAdapter adapter);
    void inject(BooksListFragment fragment);
}
