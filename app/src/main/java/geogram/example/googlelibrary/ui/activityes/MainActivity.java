package geogram.example.googlelibrary.ui.activityes;

import android.app.SearchManager;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import geogram.example.googlelibrary.R;
import geogram.example.googlelibrary.ui.fragments.BooksListFragment;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    private OnMenuItemClick onMenuItemClick;
    private BooksListFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = (BooksListFragment) fragmentManager.findFragmentById(R.id.list_container);
        if (fragment == null) {//проверяем существование фрагмента
            fragment = new BooksListFragment();//создаем новый фрагмент при необходимости
        }
        onMenuItemClick = (OnMenuItemClick) fragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.list_container, fragment)
                .commitAllowingStateLoss();


    }

    public interface OnMenuItemClick {
        void onrefresh();
        void onSearch(String search);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (query != null && !query.equals("")) {//проверяем строку поиска
                        onMenuItemClick.onSearch(query);//передаем строку поиска во фрагмент для
                        // запроса в интернет
                        searchView.clearFocus();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.enter_book_name), Toast.LENGTH_SHORT).show();
                          //показываем увидомлении о пустоте строки запроса
                    }
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);//устанавливаем слушатель строки поиска
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                onMenuItemClick.onrefresh();//обновляем список во фрагменте при нажатии на конку
                // обновления в toolbar
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
