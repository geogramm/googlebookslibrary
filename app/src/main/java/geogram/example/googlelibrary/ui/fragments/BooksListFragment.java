package geogram.example.googlelibrary.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import geogram.example.googlelibrary.MyApplication;
import geogram.example.googlelibrary.R;
import geogram.example.googlelibrary.adapters.BooksListAdapter;
import geogram.example.googlelibrary.rest.api.BooksService;
import geogram.example.googlelibrary.rest.models.Item;
import geogram.example.googlelibrary.rest.models.MainBookModel;
import geogram.example.googlelibrary.ui.activityes.MainActivity;
import geogram.example.googlelibrary.ui.activityes.SingleBookActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by geogr on 15.05.2018.
 */

public class BooksListFragment extends android.support.v4.app.Fragment implements MainActivity.OnMenuItemClick {
    @Inject
    BooksService service;
    @BindView(R.id.booksList)
    RecyclerView listView;
    private BooksListAdapter adapter;
    private String requestString = "Classic";
    private ProgressDialog progressDialog;
    private boolean loadStart = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_list, container, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("key", "key");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getsApplicationComponent().inject(this);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);
        if (savedInstanceState == null) {//проверяем создан ли фрагмент заново или восстановлен,
            // если восстановлен, например при переворте экрана используем старый адаптер
            adapter = new BooksListAdapter();
            getBooks(requestString, 0);
        }
        listView.setAdapter(adapter);
        listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {//добавляем слушатель прокрутки
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = layoutManager.getItemCount();
                int visibleItemCount = layoutManager.getChildCount();
                int firstVisibleItems = layoutManager.findFirstVisibleItemPosition();
                if (!loadStart) {
                    if ((visibleItemCount + firstVisibleItems) >= totalItemCount &&
                            getInternetConnection()) {
                        getBooks(requestString, totalItemCount);//получаем новые книги при свайпе вниз
                    }
                }
            }
        });


        listView.setOnTouchListener(new View.OnTouchListener() {//добавляем слушатель касаний по элементам списка
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        });
    }

    @Override
    public void onrefresh() {
        adapter.clear();//очищаем адаптер списка
        getBooks(requestString, 0);//обновляем список книг
    }

    @Override
    public void onSearch(String search) {
        adapter.clear();//очищаем адаптер списка
        getBooks(search, 0);//получаем список книг при новом поисковом запросе
        requestString = search;//устанавливаем значение текущего запроса,
        // при необходимости перезагрузки списка используем его
    }

    private void getBooks(String search, int offset) {
        Call<MainBookModel> call = service.get_books(search, offset);
        if (getInternetConnection()) {//проверяем интернет соединение перед запросом в интернет
            progressDialog.setMessage(getString(R.string.loading_data));
            progressDialog.show();//покахываем progressdialog при загрузке ервые элементов списка
            loadStart = true;//устанавливаем флаг начала загрузки, пока загрузка не закончится
            // ,новые элементы не загружаются
            call.enqueue(new Callback<MainBookModel>() {
                @Override
                public void onResponse(@NonNull Call<MainBookModel> call, @NonNull Response<MainBookModel> response) {
                    if (response.isSuccessful()) {//проверяем успешность запроса
                        MainBookModel res = response.body();
                        if (res != null && res.getItems() != null) {
                            List<Item> items = res.getItems();
                            if (items != null) {
                                adapter.addAll(items);//добавляем новые книги в адаптер
                                loadStart = false;//устанавливаем флаг загрузки false,
                                // чтобы можно было начать загрузку новых элементов
                                progressDialog.dismiss();//убираем progressdialog после удачной загрузки
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<MainBookModel> call, Throwable t) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                    //показываем сообщение об ошибке, если возникают проблемы при загруке списка
                }
            });
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            //показываем сообщение об ошибке, если возникают проблемы с интернетом
            loadStart = false;
        }
    }

    final GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            Item item = adapter.getItem(listView.getChildLayoutPosition(listView.findChildViewUnder(e.getX(), e.getY())));
            Intent intent = new Intent(getContext(), SingleBookActivity.class);
            try {
                intent.putExtra("pictureLink", item.getVolumeInfo().getImageLinks().getThumbnail());
            } catch (Exception ignored) {
            }
            List<String> authors = item.getVolumeInfo().getAuthors();
            String authorString = null;
            if (authors != null) {
                for (String author : authors) {
                    authorString = author + " ";
                }
            }
            intent.putExtra("title", item.getVolumeInfo().getTitle());
            intent.putExtra("description", item.getVolumeInfo().getDescription());
            intent.putExtra("author", authorString);
            intent.putExtra("bookLink", item.getVolumeInfo().getInfoLink());
            startActivity(intent);//запускаем активность с подробным описанием книги при нажатии
            // на элемент списка книг и передаем в нее необходимые данные
            return super.onSingleTapConfirmed(e);
        }
    });

    public boolean getInternetConnection() { //проверка интернет соединения
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = null;
        if (cm != null) {
            wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        if (cm != null) {
            wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        }
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        if (cm != null) {
            wifiInfo = cm.getActiveNetworkInfo();
        }
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
