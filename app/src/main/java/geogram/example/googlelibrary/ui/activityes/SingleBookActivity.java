package geogram.example.googlelibrary.ui.activityes;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import geogram.example.googlelibrary.R;

public class SingleBookActivity extends AppCompatActivity {
    @BindView(R.id.singleBookImage)
    ImageView imageView;
    @BindView(R.id.bookTitle)
    TextView bookTitle;
    @BindView(R.id.bookDescription)
    TextView bookDescription;
    @BindView(R.id.bookAuthor)
    TextView bookAuthor;
    @BindView(R.id.buyBookButton)
    Button buyBookButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_book);
        ButterKnife.bind(this);

        String title = getIntent().getStringExtra("title");
        String description = getIntent().getStringExtra("description");
        String author=getIntent().getStringExtra("author");
        final String selfLink = getIntent().getStringExtra("bookLink");

        String pictureLink = getIntent().getStringExtra("pictureLink");
        if (pictureLink != null) {//проверяем существование ссылки на обложку книги
            Picasso.with(getApplicationContext())//загружаем обложку книги в ImageView
                    .load(pictureLink)
                    .into(imageView);
        } else {
            Picasso.with(getApplicationContext())//загружаем заглушку обложки книги в ImageView
                    .load(R.drawable.placeholder_book)
                    .into(imageView);
        }
        if(title!=null&&!title.equals(""))//проверяем целостность переданных данных
        bookTitle.setText(title);//устанавливаем название книги в соответствующий TextView
        if(author!=null&&!author.equals(""))
        bookAuthor.setText(getString(R.string.author)+author);
        if(description!=null&&!description.equals(""))
        bookDescription.setText(description);
        buyBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selfLink != null) {
                    Uri address = Uri.parse(selfLink);
                    Intent openlink = new Intent(Intent.ACTION_VIEW, address);
                    startActivity(openlink);//запускаем браузре или googleplay с сылкой на покупку
                    // или скачивание необходимой книги
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //показываем ошибку, при обнаружении проблемм с ссылкой
                }
            }
        });
    }
}
