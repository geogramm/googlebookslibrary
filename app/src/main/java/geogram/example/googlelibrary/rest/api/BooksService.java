package geogram.example.googlelibrary.rest.api;


import geogram.example.googlelibrary.rest.models.MainBookModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by geogr on 15.05.2018.
 */

public interface BooksService {
    @GET("volumes")
    Call<MainBookModel> get_books(@Query("q")String request, @Query("startIndex") int offset);
}
