package geogram.example.googlelibrary.adapters.holders;

import android.view.View;

import geogram.example.googlelibrary.R;

/**
 * Created by geogr on 16.05.2018.
 */
public class BooksListViewHolder extends BaseBooksListViewHolder {


    public BooksListViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.bookViewImageItem);
        textViewTitle = itemView.findViewById(R.id.bookViewTextItem);
        textViewAuthor = itemView.findViewById(R.id.bookAuthorTextItem);
    }
}
