package geogram.example.googlelibrary.adapters.holders;

import android.view.View;

import geogram.example.googlelibrary.R;

/**
 * Created by geogr on 16.05.2018.
 */
public class BooksListViewHolderThird extends BaseBooksListViewHolder {

    public BooksListViewHolderThird(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.bookViewImageItemThird);
        textViewTitle = itemView.findViewById(R.id.bookViewTextItemThird);
        textViewAuthor = itemView.findViewById(R.id.bookAuthorTextItemThird);
    }
}
