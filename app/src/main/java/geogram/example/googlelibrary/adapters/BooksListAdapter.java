package geogram.example.googlelibrary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import geogram.example.googlelibrary.MyApplication;
import geogram.example.googlelibrary.R;
import geogram.example.googlelibrary.adapters.holders.BaseBooksListViewHolder;
import geogram.example.googlelibrary.adapters.holders.BooksListViewHolder;
import geogram.example.googlelibrary.adapters.holders.BooksListViewHolderSecond;
import geogram.example.googlelibrary.adapters.holders.BooksListViewHolderThird;
import geogram.example.googlelibrary.rest.models.Item;

/**
 * Created by geogr on 15.05.2018.
 */

public class BooksListAdapter extends RecyclerView.Adapter {
    private List<Item> books = new ArrayList<>();
    @Inject
    Context context;

    public BooksListAdapter() {
        MyApplication.getsApplicationComponent().inject(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View itemLayoutView;
        switch (viewType) {
            case 0:
                itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, null);
                holder = new BooksListViewHolder(itemLayoutView);
                break;
            case 1:
                itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item_second, null);
                holder = new BooksListViewHolderSecond(itemLayoutView);
                break;
            case 2:
                itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item_third, null);
                holder = new BooksListViewHolderThird(itemLayoutView);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (this.getItemViewType(position)) {
            case 0:
                BooksListViewHolder holder1 = (BooksListViewHolder) holder;
                setHolder(holder1, position);
                break;
            case 1:
                BooksListViewHolderSecond holder2 = (BooksListViewHolderSecond) holder;
                setHolder(holder2, position);
                break;
            case 2:
                BooksListViewHolderThird holder3 = (BooksListViewHolderThird) holder;
                setHolder(holder3, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (books == null)
            return 0;
        return books.size();
    }

    private void setHolder(BaseBooksListViewHolder holder, int position) {
        try {
            String link = books.get(position).getVolumeInfo().getImageLinks().getSmallThumbnail();
            Picasso.with(context)
                    .load(link)
                    .fit().centerCrop()
                    .placeholder(R.drawable.placeholder_book)
                    .error(R.drawable.placeholder_book)
                    .into(holder.imageView);
        }catch (Exception e){
            Picasso.with(context)
                    .load(R.drawable.placeholder_book)
                    .into(holder.imageView);
        }
        String title=books.get(position).getVolumeInfo().getTitle();
        if(title!=null)
        holder.textViewTitle.setText(books.get(position).getVolumeInfo().getTitle());
        List<String> authors = books.get(position).getVolumeInfo().getAuthors();
        String authorString = null;
        if (authors != null) {
            for (String author : authors) {
                authorString = author + " ";
            }
            holder.textViewAuthor.setText(authorString);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int decision;
        if (position >= 7) {
            int a = position / 7;
            int b = 7 * a;
            int c = position - b;
            decision = c / 3;
        } else {
            decision = position / 3;
        }
        switch (decision) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return 0;
        }
    }


    public void addAll(List<Item> list) {
        books.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        books.clear();
        notifyDataSetChanged();
    }

    public Item getItem(int position) {
        return books.get(position);
    }

}
