package geogram.example.googlelibrary.adapters.holders;

import android.view.View;

import geogram.example.googlelibrary.R;

/**
 * Created by geogr on 16.05.2018.
 */
public class BooksListViewHolderSecond extends BaseBooksListViewHolder {

    public BooksListViewHolderSecond(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.bookViewImageItemSecond);
        textViewTitle = itemView.findViewById(R.id.bookViewTextItemSecond);
        textViewAuthor = itemView.findViewById(R.id.bookAuthorTextItemSecond);
    }
}
