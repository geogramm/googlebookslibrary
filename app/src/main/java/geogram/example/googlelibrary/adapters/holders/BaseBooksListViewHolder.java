package geogram.example.googlelibrary.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by geogr on 16.05.2018.
 */
public class BaseBooksListViewHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public TextView textViewTitle;
    public TextView textViewAuthor;

    BaseBooksListViewHolder(View itemView) {
        super(itemView);
    }
}
