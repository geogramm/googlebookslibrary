package geogram.example.googlelibrary;

import android.app.Application;

import geogram.example.googlelibrary.di.component.ApplicationComponent;
import geogram.example.googlelibrary.di.component.DaggerApplicationComponent;
import geogram.example.googlelibrary.di.modules.ApplicationModule;

/**
 * Created by geogr on 15.05.2018.
 */

public class MyApplication extends Application {
    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
    }

    private void initComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }
    public static ApplicationComponent getsApplicationComponent() {
        return applicationComponent;
    }
}
